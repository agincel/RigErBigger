// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Boilerplate C++ definitions for a single module.
	This is automatically generated by UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "RigerBigger.h"
#include "RigerBigger.generated.dep.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRigerBigger() {}
	void ASplitscreenRemover::StaticRegisterNativesASplitscreenRemover()
	{
		FNativeFunctionRegistrar::RegisterFunction(ASplitscreenRemover::StaticClass(),"RemoveLocalSplitscreenPlayersFromGame",(Native)&ASplitscreenRemover::execRemoveLocalSplitscreenPlayersFromGame);
	}
	IMPLEMENT_CLASS(ASplitscreenRemover, 1753511690);
#if USE_COMPILED_IN_NATIVES
// Cross Module References
	ENGINE_API class UClass* Z_Construct_UClass_UGameInstance_NoRegister();
	ENGINE_API class UClass* Z_Construct_UClass_AActor();

	RIGERBIGGER_API class UFunction* Z_Construct_UFunction_ASplitscreenRemover_RemoveLocalSplitscreenPlayersFromGame();
	RIGERBIGGER_API class UClass* Z_Construct_UClass_ASplitscreenRemover_NoRegister();
	RIGERBIGGER_API class UClass* Z_Construct_UClass_ASplitscreenRemover();
	RIGERBIGGER_API class UPackage* Z_Construct_UPackage_RigerBigger();
	UFunction* Z_Construct_UFunction_ASplitscreenRemover_RemoveLocalSplitscreenPlayersFromGame()
	{
		struct SplitscreenRemover_eventRemoveLocalSplitscreenPlayersFromGame_Parms
		{
			UGameInstance* CurrentGameInstance;
		};
		UObject* Outer=Z_Construct_UClass_ASplitscreenRemover();
		static UFunction* ReturnFunction = NULL;
		if (!ReturnFunction)
		{
			ReturnFunction = new(EC_InternalUseOnlyConstructor, Outer, TEXT("RemoveLocalSplitscreenPlayersFromGame"), RF_Public|RF_Transient|RF_Native) UFunction(FObjectInitializer(), NULL, 0x04022401, 65535, sizeof(SplitscreenRemover_eventRemoveLocalSplitscreenPlayersFromGame_Parms));
			UProperty* NewProp_CurrentGameInstance = new(EC_InternalUseOnlyConstructor, ReturnFunction, TEXT("CurrentGameInstance"), RF_Public|RF_Transient|RF_Native) UObjectProperty(CPP_PROPERTY_BASE(CurrentGameInstance, SplitscreenRemover_eventRemoveLocalSplitscreenPlayersFromGame_Parms), 0x0000000000000080, Z_Construct_UClass_UGameInstance_NoRegister());
			ReturnFunction->Bind();
			ReturnFunction->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnFunction->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnFunction, TEXT("Category"), TEXT("Multiplayer"));
			MetaData->SetValue(ReturnFunction, TEXT("ModuleRelativePath"), TEXT("Public/SplitscreenRemover.h"));
			MetaData->SetValue(ReturnFunction, TEXT("ToolTip"), TEXT("Removes local splitscreen players, with Player Index 1 to 3, from the game, leaving only the primary player"));
#endif
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ASplitscreenRemover_NoRegister()
	{
		return ASplitscreenRemover::StaticClass();
	}
	UClass* Z_Construct_UClass_ASplitscreenRemover()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AActor();
			Z_Construct_UPackage_RigerBigger();
			OuterClass = ASplitscreenRemover::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= 0x20900080;

				OuterClass->LinkChild(Z_Construct_UFunction_ASplitscreenRemover_RemoveLocalSplitscreenPlayersFromGame());

				OuterClass->AddFunctionToFunctionMap(Z_Construct_UFunction_ASplitscreenRemover_RemoveLocalSplitscreenPlayersFromGame()); // 2078886115
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("SplitscreenRemover.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Public/SplitscreenRemover.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASplitscreenRemover(Z_Construct_UClass_ASplitscreenRemover, TEXT("ASplitscreenRemover"));
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASplitscreenRemover);
	UPackage* Z_Construct_UPackage_RigerBigger()
	{
		static UPackage* ReturnPackage = NULL;
		if (!ReturnPackage)
		{
			ReturnPackage = CastChecked<UPackage>(StaticFindObjectFast(UPackage::StaticClass(), NULL, FName(TEXT("/Script/RigerBigger")), false, false));
			ReturnPackage->PackageFlags |= PKG_CompiledIn | 0x00000000;
			FGuid Guid;
			Guid.A = 0x345CFC89;
			Guid.B = 0x5BC1584B;
			Guid.C = 0x00000000;
			Guid.D = 0x00000000;
			ReturnPackage->SetGuid(Guid);

		}
		return ReturnPackage;
	}
#endif

PRAGMA_ENABLE_DEPRECATION_WARNINGS
